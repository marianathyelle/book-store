# book-store
A simple store of books with React, Mobx and TypeScript.

# Get Start
- $ git clone https://gitlab.com/marianathyelle/book-store.git
- $ cd book-store
- $ yarn install
- $ yarn start

Open a new window on terminal and run:
- $ cd book-store
- $ yarn server
