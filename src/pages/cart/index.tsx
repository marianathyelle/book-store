import * as React from 'react';
import { Container, Row, Item, Delete, Info } from './styles';
import { CartStore } from '../../stores/cart';
import EmptyCart from '../../components/EmptyCart';
import DeleteIcon from '../../assets/images/close.svg';
import { inject, observer } from 'mobx-react';
import Total from '../../components/Total';

interface InjectedProps {
  cart: CartStore;
}

@inject("cart")
@observer
class Cart extends React.Component {
  get injected() {
    return this.props as InjectedProps;
  }

  componentDidMount() {
    if(this.injected.cart.cart.length > 0) {
      this.getValues();
    }
  }

  getIndexOfBook = (id: number) => {
    const book = this.injected.cart.cart.find(item => (
      item.id === id
    ));
    this.injected.cart.removeBookFromCart(book!)
    this.getValues();
  }

  getValues = () => {
    const booksQuantity: number[] = this.injected.cart.cart.map(book => (
      book.qtd
    ));

    const subtotalPrice: number[] = this.injected.cart.cart.map(book => (
      Number(book.subtotal)
    ));

    this.injected.cart.getTotal(booksQuantity, subtotalPrice);
  }

  render() {
    return(
      <Container>
        <h1>Your books</h1>
        {this.injected.cart.cart.length === 0 ? (
          <EmptyCart/>
        ) : (
          <React.Fragment>
            <Row>
              {this.injected.cart.cart.map(book => (
                <Item key={book.id}>
                  <img src={book.thumbnail} alt={book.title} />
                  <Info>
                    <strong>{book.title}</strong>
                    <Delete onClick={() => this.getIndexOfBook(book.id)}>
                      <img src={DeleteIcon} alt="Delete book"/>
                    </Delete>
                    <div>
                      <span>Price:</span>
                      <span>R$ {parseFloat(book.price).toFixed(2)}</span>
                    </div>
                    <div>
                      <span>Quantity:</span>
                      <span>{book.qtd}</span>
                    </div>
                    <div>
                      <span>Subtotal:</span>
                      <span>R$ {parseFloat(book.subtotal).toFixed(2)}</span>
                    </div>
                  </Info>
                </Item>
              ))}
            </Row>
            {this.injected.cart.totalDetails && (
              <Total totalDetails={this.injected.cart.totalDetails}/>
            )}
          </React.Fragment>
        ) }
      </Container>
    )
  }
}

export default Cart;