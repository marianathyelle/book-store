import * as React from 'react';
import FeaturedBooks from '../../components/FeaturedBooks';
import BooksList from '../../components/BooksList';
import { Container } from './styles';

const Main = () => (
  <Container>
    <FeaturedBooks/>
    <BooksList/>
  </Container>
)

export default Main;