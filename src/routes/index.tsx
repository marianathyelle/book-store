import * as React from 'react';
import { Switch, Route } from 'react-router-dom';
import Main from '../pages/main';
import BookDetails from '../pages/bookDetails';
import Cart from '../pages/cart';

const Routes = () => (
  <Switch>
    <Route exact path="/" component={Main} />
    <Route path="/book/:id" component={BookDetails} />
    <Route exact path="/cart" component={Cart} />
  </Switch>
);

export default Routes;