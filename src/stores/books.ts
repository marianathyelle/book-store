import { observable } from "mobx";
import { API_URL } from "../services";

export interface Book {
  id: number;
  title: string;
  author: string;
  description: string;
  thumbnail: string;
  price: string;
  featured?: boolean;
  primaryColor?: string;
  secondaryColor?: string;
}

export class BookStore {
  @observable list: Book[] = [];
  @observable book: Book = {} as Book;
  @observable isLoading: boolean = false;

  public async getBooks() {
    this.isLoading = true;

    try {
      const response = await fetch(`${API_URL}/books`);
      const data: Book[] = await response.json();
      this.list = data;

    } catch(error) {
      console.log(error)
    } finally {
      this.isLoading = false;
    }
  };

  public async getBookData(id: string) {
    this.isLoading = true;

    try {
      const response = await fetch(`${API_URL}/books/${id}`);
      const data: Book = await response.json();
      this.book = data;

    } catch(error) {
      console.log(error)
    } finally {
      this.isLoading = false;
    }
  }
}