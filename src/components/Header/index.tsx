import * as React from 'react';
import { Container, Logo, Nav } from './styles';
import { Link } from 'react-router-dom';
import { inject, observer } from 'mobx-react';
import { CartStore } from '../../stores/cart';

interface InjectedProps {
  cart: CartStore;
}

const Header = (props: any) => {
  const injected = props as InjectedProps;

  return(
    <Container>
      <Logo to='/'>BookStore</Logo>
      <Nav>
        <li>
          <Link to='/'>Store</Link>
        </li>
        <li>
          <Link to='/cart'>Cart</Link>
          {injected.cart.cart.length > 0 && <span>{injected.cart.cart.length}</span>}
        </li>
      </Nav>
    </Container>
  );
}

export default inject("cart")(observer(Header));